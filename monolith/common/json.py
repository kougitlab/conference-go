from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DateTimeEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateTimeEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}  # needed this for location encoder

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):  # lines 26 and 27 are for query
                d["href"] = o.get_api_url()
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:  # line 30-32 are for locations
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))  # for edge case
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}


# this edge case return empty dict, escape hatch
